# Itech-Docker-Schulung

## Informationen zur virtuellen Maschine
## Accountdaten
User: vagrant
Passwort: vagrant

## Gnome Desktop starten
startx

## Ein paar nützliche Quellen
### Docker Installation für mögliche Distributionen
https://docs.docker.com/engine/installation/

### Eine lokale Registry deployen
https://docs.docker.com/registry/deploying/

### Tutorials
#### Einstieg
https://github.com/docker/labs/tree/master/beginner/

#### docker-compose Python Applikation
https://docs.docker.com/compose/gettingstarted/

#### docker-compose Wordpress
https://docs.docker.com/compose/wordpress/



#### Docker Installation Ubuntu
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

## Docker benötigt Superuser-Rechte
Der Docker-Daemon benötigt Superuser-Rechte. Ihr müsst sicherstellen, dass ihr einen
Nutzer habt, der SUDO-Befehle ausführen kann. SUDO steht für "superuser do".

Test:
- 'sudo' eingeben
- Das sudo-Passwort für den Vagrant-Nutzer ist 'vagrant'

Der einfachhalthalber werden wir für diese Übung den Root-User verwenden.
- sudo su


## Lokale Images anzeigen lassen
- docker Images

## Lokale Container anzeigen lassen
Laufende Container
- docker ps

Alle Container
- docker ps -a

## Eine lokale Registry starten
- docker run -d -p 5000:5000 --restart=always --name registry registry:2

siehe auch https://docs.docker.com/registry/deploying/


## Inhalt der lokalen Registry prüfen
- curl http://localhost:5000/v2/_catalog


"cURL (ausgeschrieben Client for URLs oder Curl URL Request Library) ist eine Programmbibliothek und ein Kommandozeilen-Programm zum Übertragen von Dateien in Rechnernetzen.[...]
Wie der ausgeschriebene Name „Client for URLs“ andeutet, ist es ein Kommandozeilen-Werkzeug zum Herunter- oder Hochladen von Dateien über eine Internetadresse, auch POST-Übertragungen sind möglich. Zu den unterstützten Protokollen gehören u. a. HTTP, HTTPS, FTP, FTPS, DICT, LDAP, RTMP und Gopher." (https://de.wikipedia.org/wiki/CURL)

Falls ihr die Ausgabe formatieren möchtet, könnt
ihr das mit dem Pipe-Symbol und dem Kommandozeilenprogramm:

curl http://localhost:5000/v2/_catalog | python -m json.tool

Das Ergebnis sollte ungefähr so aussehen:

{
    "repositories": [
        "alpine"
    ]
}

## Einen Container zur lokalen Registry hinzufügen
### Pullen und taggen des Images
- docker pull alpine && docker tag alpine localhost:5000/alpine

### Zur lokalen Registry pushen
- docker push localhost:5000/alpine


## Einen Container von der lokalen Registry pullen
docker pull localhost:5000/alpine

'Localhost:5000' ist der Name Registry, 'alpine' der Name des Images

## Einen eigenen Container bauen
Gehe zum vorbereiteten Dockerfile
- https://gitlab.com/heiko.meiwes/itech-docker-schulung/blob/master/resources/docker/itech-docker-schulung/Dockerfile

Dockerfile öffnen
- gedit Dockerfile

Füge eine Datei zum Image hinzu, dazu musst du das Dockerfile anpassen und folgendes
einfügen
- RUN touch meineDatei

Baue den Container
- docker build -t localhost/mein-image:latest -t localhost/mein-image:0.0.1 .

Docker build sorgt dafür, dass das Image gebaut wird. Mit '-t' wird das Image
getaggt, also mit einem Namen versehen. Die Version 0.0.1 ist zugleich die aktuellste Version
Was zeigt dir die Ausgabe? Kannst du einzelne Steps erkennen?

Die Images anzeigen lassen und prüfen, ob die Images existieren
- docker images


Einen Container aus dem gebauten Image erzeugen
- docker run -it localhost/mein-image:latest \ash
'-it' und '\ash' ermöglichen eine interaktive Terminalsitzung mit der Shell 'ash'

Der Container läuft jetzt und ihr befindet euch innerhalb des Containers. Ihr könnt
prüfen, ob eure Datei vorhanden ist
- cd /   -> geht zum Root-Verzeichnis
- pwd    -> zeigt euch den aktuellen Pfad an
- ls     -> zeigt alle Dateien des Ordners an

Ihr könnt zudem ein Python-Terminal öffnen und eine Ausgabe erzeugen
- python
- print ("Hallo Welt!")

Den Container verlassen
- exit() -> zum Verlassen des Python-Terminals
- exit   -> zum Verlassen des Containers


Den Container pushen, also der lokalen Registry zur Verfügung stellen
- docker push localhost/mein-image:0.0.1
- docker push localhost/mein-image:latest

Die Registry prüfen
- curl http://localhost:5000/v2/_catalog




## Ein Dockernetzwerk starten
docker network create --driver bridge isolated_nw

Netzwerk anzeigen
docker network ls

Ein Netzwerk starten


Zwei Container des hallo-welt-Images zum Netzwerk hinzufügen
docker run --name=hallo-welt-1 --network=isolated_nw -t -d itech-docker-schulung/hallo-welt:0.0.1
docker run --name=hallo-welt-2 --network=isolated_nw -t -d itech-docker-schulung/hallo-welt:0.0.1

- Ein Netzwerk starten
- zwei Instanzen des Images starten und zu einem Netzwerkhinzufügen
- Das Netzwerk inspizieren
- Die Container inspizieren

Kopfnuss:
- Springe in einen der Container und 'pinge' den anderen Container. Nutze dazu
Infos der Inspect-Befehle.


## Troubleshooting
### Docker Hilfe
Globale Hilfe
docker --help

Kommando Hilfe
docker <Kommando> --help


### Docker-Daemon
Prüfen, ob der Docker-Daemon  läuft
service docker status

Den Docker-Daemon starten
service docker start

### Registry
Prüfen, ob die Registry läuft
docker ps --> es sollte ein Image registry:2 laufen
